<?php
namespace Unitylink\Remitone;

use GuzzleHttp\Client as guzzleClient;

class Client
{
    private $client;
    private $config;

    public function __construct(String $baseUrl, Array $config = [])
    {
        $this->config = $config;
        $this->client = new guzzleClient([
            'base_uri' => $baseUrl
        ]); 
    }

    public function makeRequest(String $path, Array $params = []) 
    {
        $paramsConfig=array_merge($this->config, $params);
        $response = $this->client->request('POST', $path,[
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => $paramsConfig
        ]);
        $simpleXml = simplexml_load_string($response->getBody());
        return json_encode($simpleXml);
    }
}