# [UnityLink RemitOne Library]()

This package is a wrapper for interacting with unity link's remitone API

## Installation

```sh
> composer require unitylink/remitone
```


## Usage

```php
use Unitylink\Remitone\Client;

$baseUrl = 'https://*****.remitone.com/ws/';
$group = '******'; //eg. rates
$method = '*******'; // eg. getRates

$path = "$group/$method";

$credentials = ['pin' => '******', 'username' => '***', 'password' => '****' ];

$parameters=[]; // eg. ['dest_country' => 'Ghana']

$client = new Client($baseUrl,$credentials);

echo $client->makeRequest($path,$parameters);
```

## Response

The library returns results in a json format

Eg.
```
{
  "responseId": "25579159",
  "status": "SUCCESS",
  "result": {
    "rate": {
      "account": "6.5",
      "cash_collection": "7.9621",
      "card": "6.5",
      "home_delivery": "5.95111111",
      "utility_bill": "6.5",
      "mobile_transfer": "6.5"
    },
    "source_country": "United Kingdom",
    "source_currency": {},
    "dest_country": "Ghana",
    "dest_currency": {}
  }
}
```

